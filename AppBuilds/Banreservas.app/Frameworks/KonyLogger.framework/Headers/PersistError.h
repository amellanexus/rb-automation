//
//  PersistError.h
//  Logger
//
//  Created by Harshini Bonam on 09/09/16.
//  Copyright © 2016 kony. All rights reserved.
//

#import "LoggerError.h"

@interface PersistError :LoggerError

@property BOOL shouldExit;

@end
