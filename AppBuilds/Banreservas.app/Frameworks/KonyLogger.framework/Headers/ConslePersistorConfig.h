//
//  ConslePersistorConfig.h
//  Logger
//
//  Created by kony on 27/04/17.
//  Copyright © 2017 kony. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IPersistor.h"

@interface ConslePersistorConfig : NSObject <IPersistor>

@end
