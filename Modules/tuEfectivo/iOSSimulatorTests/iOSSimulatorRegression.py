from Modules.tuEfectivo.iOSSimulatorTests import commonBehavior, historicoFlow, depositoFlow
from Modules.tuEfectivo.iOStests import retiroFlow, envioFlow


def init(driver):

    # Login automation
    commonBehavior.loginAutomation(driver)

    # Retiro Flow
    commonBehavior.openHamburger(driver)
    retiroFlow.retiroAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Deposito Flow
    commonBehavior.openHamburger(driver)
    depositoFlow.depositoAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Envio Flow
    commonBehavior.openHamburger(driver)
    envioFlow.envioAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Historico Flow
    commonBehavior.openHamburger(driver)
    historicoFlow.historicoAutomation(driver)
    commonBehavior.goToProducts(driver)
