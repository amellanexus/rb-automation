import time
from Modules.tuEfectivo.iOStests import commonBehavior
from appium.webdriver.common.touch_action import TouchAction


def loginAutomation(driver):
    commonBehavior.loginAutomation(driver)


def openHamburger(driver):
    time.sleep(10)
    TouchAction(driver).tap(x=26, y=75).perform()

def goToProducts(driver):
    commonBehavior.goToProductsAlt(driver)
