import time
from Modules.tuEfectivo.iOStests import depositoFlow
from appium.webdriver.common.touch_action import TouchAction


def depositoAutomation(driver):
    depositoFlow.depositoFields(driver)
    time.sleep(2)
    TouchAction(driver).tap(x=280, y=742).perform()