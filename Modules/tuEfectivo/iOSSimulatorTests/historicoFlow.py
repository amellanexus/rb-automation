import time

from appium.webdriver.common.touch_action import TouchAction
from Modules.tuEfectivo.iOStests import historicoFlow


def historicoAutomation(driver):
    historicoFlow.getIntoHistorico(driver)
    time.sleep(3)
    TouchAction(driver).tap(x=300, y=225).perform()
    time.sleep(3)
    TouchAction(driver).tap(x=95, y=223).perform()
    time.sleep(3)
    TouchAction(driver).tap(x=186, y=748).perform()
    driver.find_element_by_accessibility_id("lblback").click()