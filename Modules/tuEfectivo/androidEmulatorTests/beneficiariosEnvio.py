from utils import utils
from appium.webdriver.common.touch_action import TouchAction

BENEFICIRY_DATA = utils.readBeneficiarioEnvioData()


def beneficiarioEnvioAutomation(driver):
    el1 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.widget.HorizontalScrollView[5]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView[4]")
    el1.click()
    el2 = driver.find_element_by_id("com.konylabs.Banreservas:id/lblSendingTuEfectivo")
    el2.click()
    el3 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnAddProdBen")
    el3.click()
    el4 = driver.find_element_by_id("com.konylabs.Banreservas:id/tbxIdNum")
    el4.click()
    el4.send_keys(BENEFICIRY_DATA["cedula"])
    TouchAction(driver).tap(x=1249, y=2255).perform()
    el5 = driver.find_element_by_id("com.konylabs.Banreservas:id/tbxProductAlias")
    el5.send_keys(BENEFICIRY_DATA["alias"])
    el6 = driver.find_element_by_id("com.konylabs.Banreservas:id/tbxBeneficiaryNumber")
    el6.send_keys(BENEFICIRY_DATA["phoneNumber"])
    driver.hide_keyboard()
    el7 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnSave")
    el7.click()
    el8 = driver.find_element_by_id("android:id/button3")
    el8.click()
    el9 = driver.find_element_by_id("com.konylabs.Banreservas:id/lblback")
    el9.click()