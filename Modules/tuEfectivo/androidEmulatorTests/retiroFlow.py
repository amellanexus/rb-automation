from Modules.tuEfectivo.androidEmulatorTests import commonBehavior
from utils import utils

RETIRO_DATA = utils.readRetiroData()

def retiroAutomation(driver):
    el1 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.widget.HorizontalScrollView[3]/android.view.ViewGroup/android.view.ViewGroup")
    el1.click()
    el2 = driver.find_element_by_id("com.konylabs.Banreservas:id/lblTuEfectivo")
    el2.click()
    el3 = driver.find_element_by_id("com.konylabs.Banreservas:id/comMenuRowOneLineWithdraw")
    el3.click()
    el4 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]")
    el4.click()
    utils.findElementByText(driver, RETIRO_DATA["account"]) # Select Account
    el6 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[1]")
    el6.click()
    commonBehavior.numKeyboardPresser(RETIRO_DATA["amount"], driver)
    utils.findElementByText(driver, "Confirmar")
    continueBtn = driver.find_element_by_id("com.konylabs.Banreservas:id/btnNext")
    continueBtn.click()
    codeStrPath = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup[1]/android.widget.EditText")
    commonBehavior.identifyVerificationNumber(driver, codeStrPath)
    driver.find_element_by_id("com.konylabs.Banreservas:id/btnNext").click() # Continue Btn
