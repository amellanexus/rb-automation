from Modules.tuEfectivo.androidEmulatorTests import envioFlow, beneficiariosEnvio, depositoFlow, historicoFlow, \
    commonBehavior, retiroFlow


def regressionTest(driver):
    # Login Automation
    commonBehavior.loginAutomation(driver)

    # Retiro Automation
    commonBehavior.openHamburgerMenu(driver)
    retiroFlow.retiroAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Deposito Automation
    commonBehavior.openHamburgerMenu(driver)
    depositoFlow.depositoAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Envio Automation
    commonBehavior.openHamburgerMenu(driver)
    envioFlow.envioAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Historico automation
    commonBehavior.openHamburgerMenu(driver)
    historicoFlow.historicoAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Beneficiario Envio Flow
    commonBehavior.openHamburgerMenu(driver)
    beneficiariosEnvio.beneficiarioEnvioAutomation(driver)
    commonBehavior.goToProducts(driver)


