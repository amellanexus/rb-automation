from utils import utils

USER_DATA = utils.readUsernameInfo()

# Login Behavior of the app. Note it closes the pop up that asks you if you want to start the debugger
def loginAutomation(driver):
    el1 = driver.find_element_by_id("android:id/button2") # Debugger pop up close button
    el1.click()
    el2 = driver.find_element_by_id("com.konylabs.Banreservas:id/txtUser")
    el2.click()
    el2.send_keys(USER_DATA["username"])
    driver.hide_keyboard()
    el3 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnContinue")
    el3.click()
    el4 = driver.find_element_by_id("com.konylabs.Banreservas:id/txtPwd")
    el4.click()
    el4.send_keys(USER_DATA["password"])
    driver.hide_keyboard()
    el5 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnLogin")
    el5.click()
    el6 = driver.find_element_by_id("com.konylabs.Banreservas:id/txtPhoneNo")
    el6.click()
    el6.send_keys(USER_DATA["phoneNumber"])
    driver.hide_keyboard()
    el7 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnContinue")
    el7.click()
    driver.find_element_by_id("android:id/button3").click()
    otpNumberTextbox = driver.find_element_by_id("com.konylabs.Banreservas:id/txtOTP")
    otpNumberTextbox.send_keys("123456")
    el8 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnContinue")
    el8.click()

    elemId = "com.konylabs.Banreservas:id/btnActivate"
    isExist = utils.isExist(driver, elemId)

    if isExist:
        setUpPreferences(driver)
    else:
        el9 = driver.find_element_by_id("com.konylabs.Banreservas:id/lblTub")
        el9.click()

def setUpPreferences(driver):
    el1 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnActivate")
    el1.click()
    el5 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnCancel")
    el5.click()
    el3 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnActivate")
    el3.click()
    utils.findElementByText(driver, "SALTAR")
    el5 = driver.find_element_by_id("com.konylabs.Banreservas:id/lblback")
    el5.click()
    el6 = driver.find_element_by_id("com.konylabs.Banreservas:id/lblTub")
    el6.click()



def openHamburgerMenu(driver):
    el3 = driver.find_element_by_id("com.konylabs.Banreservas:id/lblHamMenu")
    el3.click()


# Press the keys of the numeric keyboard based on a num passed
def numKeyboardPresser(num, driver):
    for char in num:
        if(char == "1"):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btnOne").click()
        elif(char == "2"):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btnTwo").click()
        elif(char == "3"):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btnthree").click()
        elif(char == "4"):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btnFour").click()
        elif(char == "5"):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btnFive").click()
        elif(char == "6"):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btnSix").click()
        elif(char == "7"):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btnSeven").click()
        elif(char == "8"):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btnEight").click()
        elif(char == "9"):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btnNine").click()
        elif(char == "0"):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btnZero").click()
        elif(char == "."):
            driver.find_element_by_id("com.konylabs.Banreservas:id/btndot").click()


# Identify the element that has the "Insertar codigo de verificacion #" text and enters
# the corresponding code. codeBoxPath is the xPath or id of the element in the screen
def identifyVerificationNumber(driver, codeBoxPath):
    codeNumber = codeBoxPath.text[-2:].strip()
    codeCard = USER_DATA["codeCard"]
    codeCardValue = codeCard[codeNumber]
    codeBoxPath.send_keys(codeCardValue)


def goToProducts(driver):
    burger = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView")
    burger.click()
    utils.findElementByText(driver, "Productos")