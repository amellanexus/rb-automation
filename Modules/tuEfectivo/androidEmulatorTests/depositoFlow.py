from utils import utils

DEPOSIT_DATA = utils.readDepositoData()


def depositoAutomation(driver):
    el1 = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.widget.HorizontalScrollView[3]/android.view.ViewGroup/android.view.ViewGroup")
    el1.click()
    el2 = driver.find_element_by_id("com.konylabs.Banreservas:id/lblTuEfectivo")
    el2.click()
    el3 = driver.find_element_by_id("com.konylabs.Banreservas:id/comMenuRowOneLineDeposit")
    el3.click()
    el4 = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]")
    el4.click()
    utils.findElementByText(driver, DEPOSIT_DATA["account"])
    el5 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnNext")
    el5.click()
    el6 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnNext")
    el6.click()

