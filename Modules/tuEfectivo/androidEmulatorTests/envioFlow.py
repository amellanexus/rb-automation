from utils import utils
from Modules.tuEfectivo.androidEmulatorTests import commonBehavior

ENVIO_DATA = utils.readEnvioData()

def envioAutomation(driver):
    el1 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.widget.HorizontalScrollView[3]/android.view.ViewGroup/android.view.ViewGroup")
    el1.click()
    el2 = driver.find_element_by_id("com.konylabs.Banreservas:id/lblTuEfectivo")
    el2.click()
    el3 = driver.find_element_by_id("com.konylabs.Banreservas:id/comMenuRowOneLineSending")
    el3.click()
    el4 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]")
    el4.click()
    utils.findElementByText(driver, ENVIO_DATA["account"])
    el5 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]")
    el5.click()
    commonBehavior.numKeyboardPresser(ENVIO_DATA["amount"], driver)
    utils.findElementByText(driver, "Confirmar")
    el6 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[1]")
    el6.click()
    utils.findElementByText(driver, ENVIO_DATA["beneficiaryName"])
    el7 = driver.find_element_by_id("com.konylabs.Banreservas:id/txtfield")
    el7.send_keys(ENVIO_DATA["concept"])
    driver.hide_keyboard()
    el9 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnNext")
    el9.click()

    fromElem = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup")
    utils.scroll(driver, fromElem)

    codeStrPath = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup[1]/android.widget.EditText")
    commonBehavior.identifyVerificationNumber(driver, codeStrPath)
    el10 = driver.find_element_by_id("com.konylabs.Banreservas:id/btnNext")
    el10.click()


