from utils import utils
from Modules.tuEfectivo.iOStests import commonBehavior
from appium.webdriver.common.touch_action import TouchAction

RETIRO_DATA = utils.readRetiroData()

def retiroAutomation(driver):
    TouchAction(driver).tap(x=91, y=316).perform()

    el1 = driver.find_element_by_accessibility_id("lblTuEfectivo")
    el1.click()
    el2 = driver.find_element_by_accessibility_id("comMenuRowOneLineWithdraw")
    el2.click()
    el3 = driver.find_element_by_xpath("(//XCUIElementTypeStaticText[@name=\"lblSubTitle\"])[1]")
    el3.click()

    utils.iOSFindElementBytext(driver, RETIRO_DATA["account"])
    el5 = driver.find_element_by_xpath("(//XCUIElementTypeOther[@name=\"flxData\"])[3]")
    el5.click()
    commonBehavior.numKeyboardPresser(driver, RETIRO_DATA["amount"])
    utils.iOSFindElementBytext(driver, "Confirmar")

    utils.iOSFindElementBytext(driver, "Continuar")
    codeBox = driver.find_element_by_xpath("//XCUIElementTypeSecureTextField[@name=\"txtfield\"]")
    commonBehavior.getCode(driver, codeBox)
    driver.hide_keyboard()
    driver.find_element_by_accessibility_id("btnNext").click()


