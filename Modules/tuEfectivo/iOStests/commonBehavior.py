import time
from utils import utils
from appium.webdriver.common.touch_action import TouchAction

USER_DATA = utils.readUsernameInfo()


def loginAutomation(driver):
    el1 = driver.find_element_by_accessibility_id("txtUser")
    el1.send_keys(USER_DATA["username"])
    driver.hide_keyboard()
    el2 = driver.find_element_by_accessibility_id("btnContinue")
    el2.click()
    el3 = driver.find_element_by_accessibility_id("txtPwd")
    el3.send_keys(USER_DATA["password"])
    driver.hide_keyboard()
    el4 = driver.find_element_by_accessibility_id("btnLogin")
    el4.click()

    el5 = driver.find_element_by_accessibility_id("txtPhoneNo")
    el5.send_keys(USER_DATA["phoneNumber"])

    TouchAction(driver).tap(x=378, y=490).perform()  # Hides numerical keyboard

    el2 = driver.find_element_by_accessibility_id("btnContinue")
    el2.click()

    el4 = driver.find_element_by_accessibility_id("txtOTP")
    el4.send_keys(USER_DATA["otpCode"])

    TouchAction(driver).tap(x=378, y=490).perform()
    el5 = driver.find_element_by_accessibility_id("btnContinue")
    el5.click()
    el6 = driver.find_element_by_accessibility_id("btnActivate")
    el6.click()
    time.sleep(10)
    el7 = driver.find_element_by_accessibility_id("btnCancel")
    el7.click()
    time.sleep(5)
    TouchAction(driver).tap(x=331, y=723).perform()
    el9 = driver.find_element_by_accessibility_id("SALTAR")
    el9.click()
    time.sleep(3)
    TouchAction(driver).tap(x=27, y=47).perform()
    time.sleep(10)
    TouchAction(driver).tap(x=100, y=67).perform()
    time.sleep(2)


def openHamburgerMenu(driver):
    time.sleep(10)
    TouchAction(driver).tap(x=25, y=52).perform()


def numKeyboardPresser(driver, amount):
    for char in amount:
        if char == "1":
            driver.find_element_by_accessibility_id("btnOne").click()
        if char == "2":
            driver.find_element_by_accessibility_id("btnTwo").click()
        if char == "3":
            driver.find_element_by_accessibility_id("btnthree").click()
        if char == "4":
            driver.find_element_by_accessibility_id("btnFour").click()
        if char == "5":
            driver.find_element_by_accessibility_id("btnFive").click()
        if char == "6":
            driver.find_element_by_accessibility_id("btnSix").click()
        if char == "7":
            driver.find_element_by_accessibility_id("btnSeven").click()
        if char == "8":
            driver.find_element_by_accessibility_id("btnEight").click()
        if char == "9":
            driver.find_element_by_accessibility_id("btnNine").click()
        if char == "0":
            driver.find_element_by_accessibility_id("btnZero").click()
        if char == ".":
            driver.find_element_by_accessibility_id("btndot").click()


def getCode(driver, codeBoxPath):
    codeNumber = codeBoxPath.text[-2:].strip()
    codeCard = USER_DATA["codeCard"]
    codeCardValue = codeCard[codeNumber]
    codeBoxPath.send_keys(codeCardValue)

def goToProducts(driver):
    openHamburgerMenu(driver)
    time.sleep(1)
    utils.iOSFindElementBytext(driver, "Productos")


def goToProductsAlt(driver):
    driver.find_element_by_accessibility_id("lblMenu").click()
    utils.iOSFindElementBytext(driver, "Productos")
