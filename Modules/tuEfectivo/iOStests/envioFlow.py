from utils import utils
from Modules.tuEfectivo.iOStests import commonBehavior

ENVIO_DATA = utils.readEnvioData()

def envioAutomation(driver):
    utils.iOSFindElementBytext(driver, "Transacciones")
    driver.find_element_by_accessibility_id("lblTuEfectivo").click()
    driver.find_element_by_accessibility_id("comMenuRowOneLineSending").click()
    driver.find_element_by_xpath("(//XCUIElementTypeStaticText[@name=\"lblSubTitle\"])[1]").click()
    utils.iOSFindElementBytext(driver, ENVIO_DATA["account"])
    driver.find_element_by_xpath("(//XCUIElementTypeOther[@name=\"flxData\"])[2]").click()
    commonBehavior.numKeyboardPresser(driver, ENVIO_DATA["amount"])
    utils.iOSFindElementBytext(driver, "Confirmar")
    driver.find_element_by_xpath("(//XCUIElementTypeOther[@name=\"flxData\"])[3]").click()
    utils.iOSFindElementBytext(driver, ENVIO_DATA["beneficiaryName"])
    driver.find_element_by_accessibility_id("txtfield").send_keys(ENVIO_DATA["concept"])
    driver.hide_keyboard()
    driver.find_element_by_accessibility_id("btnNext").click()
    codeBox = driver.find_element_by_xpath("//XCUIElementTypeSecureTextField[@name=\"txtfield\"]")
    commonBehavior.getCode(driver, codeBox)
    driver.hide_keyboard()
    driver.find_element_by_accessibility_id("btnNext").click()








