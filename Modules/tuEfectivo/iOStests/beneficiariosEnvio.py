import time
from utils import utils
from appium.webdriver.common.touch_action import TouchAction

BENEFICIARIO_DATA = utils.readBeneficiarioEnvioData()

def beneficiarioEnvioAutomation(driver):
    utils.iOSFindElementBytext(driver, "Configuraciones")
    driver.find_element_by_accessibility_id("flexSendingTuEfectivo").click()
    time.sleep(5)
    TouchAction(driver).tap(x=203, y=709).perform()
    driver.find_element_by_accessibility_id("tbxIdNum").send_keys(BENEFICIARIO_DATA["cedula"])
    driver.hide_keyboard()
    time.sleep(5)
    TouchAction(driver).tap(x=194, y=383).perform()
    driver.find_element_by_accessibility_id("tbxProductAlias").send_keys(BENEFICIARIO_DATA["alias"])
    driver.hide_keyboard()
    driver.find_element_by_accessibility_id("tbxBeneficiaryNumber").click()
    driver.find_element_by_accessibility_id("tbxBeneficiaryNumber").send_keys(BENEFICIARIO_DATA["phoneNumber"])
    driver.hide_keyboard()
    time.sleep(2)
    TouchAction(driver).tap(x=212, y=711).perform()
    time.sleep(6)
    TouchAction(driver).tap(x=27, y=34).perform()









