from utils import utils
from appium.webdriver.common.touch_action import TouchAction
import time

DEPOSITO_DATA = utils.readDepositoData()


def depositoAutomation(driver):
    depositoFields(driver)
    time.sleep(5)
    TouchAction(driver).tap(x=331, y=707).perform()

def depositoFields(driver):
    utils.iOSFindElementBytext(driver, "Transacciones")
    driver.find_element_by_accessibility_id("lblTuEfectivo").click()
    driver.find_element_by_accessibility_id("comMenuRowOneLineDeposit").click()
    driver.find_element_by_accessibility_id("lblSubTitle").click()
    utils.iOSFindElementBytext(driver, DEPOSITO_DATA["account"])
    el1 = driver.find_element_by_accessibility_id("Continuar")
    el1.click()






