from Modules.tuEfectivo.iOStests import historicoFlow, retiroFlow, beneficiariosEnvio, commonBehavior, depositoFlow, \
    envioFlow


def init(driver):

    # Login flow
    commonBehavior.loginAutomation(driver)

    # Retiro flow
    commonBehavior.openHamburgerMenu(driver)
    retiroFlow.retiroAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Deposito Flow
    commonBehavior.openHamburgerMenu(driver)
    depositoFlow.depositoAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Envio Flow
    commonBehavior.openHamburgerMenu(driver)
    envioFlow.envioAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Historico Flow
    commonBehavior.openHamburgerMenu(driver)
    historicoFlow.historicoAutomation(driver)
    commonBehavior.goToProducts(driver)

    # Beneficiario Envio Flow
    commonBehavior.openHamburgerMenu(driver)
    beneficiariosEnvio.beneficiarioEnvioAutomation(driver)
    commonBehavior.goToProductsAlt(driver)

