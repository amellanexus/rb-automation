### How to run

1. Need to have an .apk file in the root directory of the project.
2. Make sure that yor emulator matches the properties on desiredCapabilities.json file
3. Make sure you enter the data for the flows in flowinfo.json
4. Start appium server
5. Run the project with pyCharm or just execute the index.py script (Python 3.8)

### Important
If you want to execute the test on iOS or Android make sure that the `platformToUse` property in `desiredCapabilities.json` 
is set to the name of the of the name of the the property of the correspondent desired capabilities.

Example: If you want to test on an iOS Real Device `platformToUse` should have this value,
look that is the same for the desired capabilities of iOS for a real device.

```buildoutcfg
{
    "platformToUse": "iOSDevice",

    "AndroidEmulator":
    {
        "platformName": "Android",
        "platformVersion": "9",
        "automationName": "uiautomator2",
        "deviceName": "emulator-5554",
        "app": "luavmandroid.apk"
    },
    "iOSDevice":
    {
        "platformName": "iOS",
        "deviceName": "iPhone",
        "app": "KRelease.ipa",
        "platformVersion": "13.3",
        "xcodeOrgId": "<Team ID>",
        "xcodeSigningId": "iPhone Developer",
        "udid": "Device Identifyer",
        "autoAcceptAlerts": true
    }

}
```
    
#### Note
Supported:

1. Android Emulator
2. iOS Real Device