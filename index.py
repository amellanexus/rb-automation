from appium import webdriver
from Modules.tuEfectivo.androidEmulatorTests import regression
from Modules.tuEfectivo.iOStests import iOSRegression
from Modules.tuEfectivo.iOSSimulatorTests import iOSSimulatorRegression
from capabilitiesConfig import desiredCapabilities


platform = desiredCapabilities["platformToUse"]
driver = webdriver.Remote("http://localhost:4723/wd/hub", desiredCapabilities[platform])
driver.implicitly_wait(30)

print(driver.get_window_size())

# Regression test for tuEfectivo feature
if desiredCapabilities['platformToUse'] == "AndroidEmulator":
    regression.regressionTest(driver)
elif desiredCapabilities['platformToUse'] == "iOSDevice":
    iOSRegression.init(driver)
elif desiredCapabilities["platformToUse"] == "iOSSimulator":
    iOSSimulatorRegression.init(driver)

