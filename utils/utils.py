import platform
import os
import json
from appium.webdriver.common.touch_action import TouchAction

def getProyectpath():
    return os.getcwd()

# Determine operating system
def adjustPathToCurrentOS(path):
    currentOS = platform.system()
    pathSlash = ""
    if currentOS == "Windows":
        pathSlash = "\\"
    if currentOS == "Darwin":
        pathSlash = "/"
    return path + pathSlash


# Reads a .JSON file in the root directory. Takes the file name as parameter
def readJSONFile(fileName):
    path = adjustPathToCurrentOS(os.getcwd())
    with open(path + fileName) as f:
        flowInfo = json.load(f)
    return flowInfo


# Reads property from flowinfo.json file
def readUsernameInfo():
    return readJSONFile("flowinfo.json")["userInfo"]


def readRetiroData():
    return readJSONFile("flowinfo.json")["retiro"]


def readDepositoData():
    return readJSONFile("flowinfo.json")["deposito"]


def readEnvioData():
    return readJSONFile("flowinfo.json")["envio"]


def readBeneficiarioEnvioData():
    return readJSONFile("flowinfo.json")["beneficiarioEnvio"]


# Finds the element based on its text. Takes the driver and the tech to search as parameter
def findElementByText(driver, textToFind):
    element = driver.find_element_by_xpath("//*[@text='" + textToFind + "']")
    element.click()


def iOSFindElementBytext(driver, textToFind):
    element = driver.find_element_by_xpath("//*[@label='" + textToFind + "']")
    element.click()


# Scrolls from a position in the screen. Takes the driver and the element object from witch the scroll
# is going to start
def scroll(driver, FromElement):
    size = driver.get_window_size()
    width = int(size["width"])
    height = int(size["height"])
    x = round(width / 2)
    endy = round(height * 0.10)
    TouchAction(driver).press(FromElement).move_to(x=x, y=endy).release().perform()


def isExist(driver, elem):
    try:
        driver.find_element_by_id(elem)
        return True
    except:
        return False

