from utils import utils

# Desired Capabilities
# Refer to http://appium.io/docs/en/writing-running-appium/caps/


desiredCapabilities = utils.readJSONFile("desiredCapabilities.json")
currentPath = utils.getProyectpath()
platform = desiredCapabilities['platformToUse']
buildPath = utils.adjustPathToCurrentOS(currentPath) + desiredCapabilities[platform]['app']
desiredCapabilities[platform]['app'] = buildPath






